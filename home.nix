{ pkgs, lib, ... }:

{
  imports = [
    modules/direnv
    modules/zsh
    modules/tmux
    # modules/alacritty
    modules/git
    # modules/spotifyd
    modules/pgcli
    modules/xdg
    modules/starship
    modules/wezterm
  ];

  config = {
    fonts.fontconfig.enable = true;

    home = {
      language.base = "en_US.UTF-8";

      packages = with pkgs;
        [
          (if stdenv.isLinux then emacs else emacsMacport)
          fd
          ripgrep
          htop
          mosh
          shellcheck
          openssh
          which
          kubectl
          iosevka
          keepassxc
          nixfmt
          du-dust
          fira-code
          tokei
          hyperfine
          sd
          tealdeer
          httpie
          ngrok
          rclone
          jo
          spotify
          procs
          slack
          mcomix3
        ] ++ lib.optional stdenv.isLinux unar;
    };

    programs = {
      home-manager.enable = true;

      jq.enable = true;

      bat.enable = true;
    };
  };
}
