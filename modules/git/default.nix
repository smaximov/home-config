{
  programs.git = {
    enable = true;

    userName = "Sergei Maximov";
    userEmail = "s.b.maximov@gmail.com";

    extraConfig = {
      github.user = "smaximov";
      gitlab.user = "smaximov";

      protocol.version = 2;

      url = {
        "git@gitlab.com:igooods" = {
          insteadOf = "https://gitlab.com/igooods";
        };
      };
    };

    aliases = {
      hist = ''
        log --all --graph --pretty=format:'%Cred%h%Creset -%C(auto)%d%Creset %s %Cgreen(%cr) %C(bold blue)<%an>%Creset' --abbrev-commit --date=relative
      '';
    };
  };
}
