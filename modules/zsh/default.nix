{ config, pkgs, lib, ... }:

{
  home.packages = with pkgs; [ less exa gnugrep jq ];

  # Force creation of the ZSH cache directory, which stores HISTFILE:
  home.file."${config.xdg.cacheHome}/zsh/.keep".text = "";

  programs.zsh = rec {
    enable = true;

    dotDir = ".config/zsh";

    enableAutosuggestions = true;

    sessionVariables = rec {
      EDITOR = "emacsclient -t";
      VISUAL = EDITOR;
      PAGER = "${pkgs.less}/bin/less -M";
    };

    shellAliases = {
      e = "nocorrect ${sessionVariables.EDITOR}";
      rm = "rm -v";
      mv = "mv -v";
      cp = "cp -v";
      l = "${pkgs.exa}/bin/exa -l";
      bundle = "noglob bundle";
      rake = "noglob rake";

      "-g L" = "| ${pkgs.less}/bin/less";
      "-g G" = "| ${pkgs.gnugrep}/bin/grep --color=auto -P";
      "-g JQ" = "2>/dev/null | ${pkgs.jq}/bin/jq";
    };

    initExtra = ''
      export PATH="$PATH:$HOME/.rvm/bin"
      export PATH="$PATH:$HOME/.local/bin"
    '';

    loginExtra = ''
      [[ -s "$HOME/.rvm/scripts/rvm" ]] && source "$HOME/.rvm/scripts/rvm"
    '';

    history = {
      extended = true;
      ignoreDups = true;
      expireDuplicatesFirst = true;
      path = ".cache/zsh/zhistory";
    };

    oh-my-zsh = {
      enable = true;

      plugins = [ "git" ];
    };
  };
}
