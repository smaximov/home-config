{ pkgs, ... }:

{
  home.packages = with pkgs; [ pgcli ];

  xdg.configFile."pgcli/config".source = ./config;
}
