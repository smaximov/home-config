{
  programs.alacritty = {
    enable = true;

    settings = {
      window.startup_mode = "Maximized";

      font = {
        normal = {
          family = "Iosevka";
          style = "Regular";
        };

        size = 8;
      };

      selection.save_to_clipboard = true;
    };
  };
}
