let stdlib = builtins.readFile ./stdlib.sh;
in {
  programs.direnv = {
    enable = true;

    inherit stdlib;
  };
}
