{ pkgs, lib, ... }:

let
  wezterm = pkgs.wezterm.overrideAttrs (oldAttrs: {
    postInstall = lib.optionalString pkgs.stdenv.isLinux ''
      install -D assets/icon/wezterm-icon.svg $out/share/icons/hicolor/scalable/apps/wezterm.svg
    '';
  });

  wezterm-desktop = pkgs.makeDesktopItem rec {
    name = "WezTerm";
    desktopName = name;
    genericName = "Terminal";
    comment = "Wez's Terminal Emulator";
    categories = "System;TerminalEmulator;";
    exec = "${wezterm}/bin/wezterm";
    icon = "${wezterm}/share/icons/hicolor/scalable/apps/wezterm.svg";

    extraDesktopEntries = {
      StartupWMClass = "org.wezfurlong.wezterm";
    };
  };
in {
  home.packages = [ wezterm wezterm-desktop ];

  xdg.configFile."wezterm/wezterm.lua".source = ./wezterm.lua;
}
