local wezterm = require 'wezterm';

return {
  color_scheme = 'Wombat',

  exit_behavior = 'Close',

  font = wezterm.font({
    family='Iosevka',
    weight='Regular'
  }),

  font_size = 8,

  keys = {
    {key='%', mods='CTRL', action=wezterm.action{SplitHorizontal={domain="CurrentPaneDomain"}}},
    {key='"', mods='CTRL', action=wezterm.action{SplitVertical={domain="CurrentPaneDomain"}}}
  },

  tab_bar_at_bottom = true,
}
