{ pkgs, lib, ... }:

{
  config = lib.mkIf pkgs.stdenv.isLinux {
    services.spotifyd = {
      enable = true;

      package = pkgs.spotifyd.override {
        withKeyring = true;
        withPulseAudio = true;
        withMpris = true;
      };

      settings = {
        global = {
          username = "hpd89hnax1t0h1fk9jy7qhq7y";
          use_keyring = "true";
          backend = "pulseaudio";
          device_name = "spotifyd";
          device_type = "computer";
          bitrate = "320";
        };
      };
    };
  };
}
