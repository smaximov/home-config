{
  programs.starship = {
    enable = true;
    enableBashIntegration = false;
    enableFishIntegration = false;
    settings = {
      time = {
        disabled = false;
        format = "🕙 %T";
      };

      kubernetes.disabled = false;
    };
  };
}
